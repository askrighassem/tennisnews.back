namespace Domain.Values
{
    public record Country
    {
        public string Picture { get; set; }
        public string Code { get; set; }
    }
}