﻿using System;
using Domain.Values;

namespace Domain
{
    public sealed class TennisPlayer
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string  ShortName { get; set; }
        public char Sex { get; set; }
        public Country Country { get; set; }
        public string Picture { get; set;}
        public DataPlayer Data { get; set;  }
        
        
    }
}