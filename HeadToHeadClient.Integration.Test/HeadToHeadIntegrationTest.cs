using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Domain;
using Domain.Values;
using FluentAssertions;
using Microsoft.Extensions.Options;
using TennisNews.Back.HeadToHead;
using Xunit;

namespace HeadToHeadClient.Integration.Test
{
    public class HeadToHeadIntegrationTest 
    {
        private readonly HttpClient _httpClient;
        private readonly IHeadToHeadClient _headToHeadClient;
        private readonly HeadToHeadClientConfiguration _headToHeadClientConfiguration ;  
        private readonly IOptions<HeadToHeadClientConfiguration> _headToHeadClientOptionsConfiguration ;
        public HeadToHeadIntegrationTest()
        {
            _httpClient = new HttpClient(); 
            _headToHeadClientConfiguration = new ()
            {
                Url = "https://data.latelier.co/training/tennis_stats/headtohead.json",
                ApiKey = ""
            };
            _headToHeadClientOptionsConfiguration = Options.Create(_headToHeadClientConfiguration); 
            _headToHeadClient = new TennisNews.Back.HeadToHead.HeadToHeadClient(_headToHeadClientOptionsConfiguration);
        }
        [Fact]
        public async Task  When_Call_Head_To_Head_Client_Should_Return_OK_Status()
        {
            var clientResponse = await _httpClient.GetAsync(_headToHeadClientConfiguration.Url);
            clientResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task  When_Call_Head_To_Head_Client_Should_Not_Return_Null()
        {
            var clientResponse = await _httpClient.GetAsync(_headToHeadClientConfiguration.Url);
            clientResponse.Content.Should().NotBeNull();
        }
        [Fact]
        public async Task  When_Call_Head_To_Head_Client_Players_Count_Should_Be_Greater_Then_One()
        {
            var clientResponse =  await _headToHeadClient.GetTennisPlayersData();
            clientResponse.Players.Count().Should().BeGreaterThan(1);
        }
        [Fact]
        public async Task  When_Call_Head_To_Head_Client_First_Player_Should_Be_Equivalent_To_The_Tennis_Player_Example()
        {
            TennisPlayer tennisPlayerExample = new()
            {
                Id = 52,
                Firstname = "Novak",
                Lastname = "Djokovic",
                ShortName = "N.DJO",
                Sex ='M',
                Country = new()
                {
                    Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                    Code = "SRB"
                },
                Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                Data = new ()
                {
                    Rank = 2,
                    Points = 2542,
                    Weight = 80000,
                    Height =188,
                    Age =31,
                    Last = new int[]{1,1,1,1,1}
                }
            };
            var clientResponse = await _headToHeadClient.GetTennisPlayersData();
            clientResponse.Players.First().Should().BeEquivalentTo(tennisPlayerExample); 
        }
    }
}