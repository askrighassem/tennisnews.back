using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;

namespace TennisNews.Back.HeadToHead
{
    public interface IHeadToHeadClient
    {
        Task<TennisPlayers> GetTennisPlayersData(); 
    }
}