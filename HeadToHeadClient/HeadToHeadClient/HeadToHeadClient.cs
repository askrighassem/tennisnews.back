﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Domain;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace TennisNews.Back.HeadToHead
{
    public sealed class HeadToHeadClient : IHeadToHeadClient
    {
        private readonly HeadToHeadClientConfiguration _headClientConfigurationSnapShot;
        public HeadToHeadClient(IOptions<HeadToHeadClientConfiguration> optionsSnapshot)
        {
            _headClientConfigurationSnapShot = optionsSnapshot.Value;
            
        }
        public async Task<TennisPlayers> GetTennisPlayersData()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var httpResponse = await httpClient.GetAsync(_headClientConfigurationSnapShot.Url); 
                    var responseContent = await httpResponse.Content.ReadAsStringAsync(); 
                    var tennisPlayers = JsonConvert.DeserializeObject<TennisPlayers>(responseContent);
                    httpClient.Dispose(); 
                    return tennisPlayers; 
                }
                
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}