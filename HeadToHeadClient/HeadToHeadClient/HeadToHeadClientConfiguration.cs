namespace TennisNews.Back.HeadToHead
{
    public class HeadToHeadClientConfiguration
    {
        public string Url { get; set; }
        public string ApiKey { get; set; }
    }
}