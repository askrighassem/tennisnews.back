using System;
using System.Threading.Tasks;
using Domain;
using Domain.Values;
using FluentAssertions;
using Moq;
using TennisNews.Back.HeadToHead;
using Xunit;

namespace Application.Test
{
    public class ApplicationTest
    {
        [Fact]
        public async Task When_Sorting_The_Tennis_Players_List_Should_Be_Sorted_Descendantly()
        {
            var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object); 
            var tennisPlayers = await tennisNews.GetTennisPlayersDescendantly(); 
            tennisPlayers.Should().BeEquivalentTo(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                        },
                        new TennisPlayer()
                        {
                                Id = 52,
                                Firstname = "Venus",
                                Lastname = "Williams",
                                ShortName = "V.WIL",
                                Sex ='F',
                                Country = new()
                                {
                                    Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                    Code = "USA"
                                },
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                                Data = new ()
                                {
                                    Rank = 52,
                                    Points = 1105,
                                    Weight = 74000,
                                    Height =185,
                                    Age =38,
                                    Last = new int[]{0, 1, 0, 0, 1}
                                }
                            
                            }
                        
                } 
            });
        }
        [Fact]
        public async Task When_Sorting_The_Tennis_Players_List_Should_Be_Not_Null()
        {
              var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object); 
            var tennisPlayers = await tennisNews.GetTennisPlayersDescendantly(); 
            tennisPlayers.Should().NotBeNull();
        }
        [Fact]
        public async Task When_Sorting_The_Tennis_Players_List_Should_Be_Greater_Than_One()
        {
              var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object); 
            var tennisPlayers = await tennisNews.GetTennisPlayersDescendantly(); 
            tennisPlayers.Players.Length.Should().BeGreaterThan(1);
        }
        [Fact]
        public async Task When_Calling_A_Player_By_Id_Player_That_Exist_Result_Should_Not_Equal_Null_Type()
        {
              var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            const int id = 17; 
            var tennisPlayer = await tennisNews.GetTennisPlayerById(id); 
            tennisPlayer.Should().NotBeNull(); 
            
        }
        [Fact]
        public async Task When_Calling_A_Player_By_Id_Result_Should_Have_The_Player_Example_Format_If_It_Exist()
        {
             var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            const int id = 17; 
            var tennisPlayerExample = new TennisPlayer()
            {
                Id = 17,
                Firstname = "Rafael",
                Lastname = "Nadal",
                ShortName = "R.NAD",
                Sex ='M',
                Country = new()
                {
                    Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                    Code = "ESP"
                },
                Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                Data = new ()
                {
                    Rank = 1,
                    Points = 1982,
                    Weight = 85000,
                    Height =185,
                    Age =33,
                    Last = new int[]{0, 1, 1, 1, 0}
                }
            }; 
            var tennisPlayer = await tennisNews.GetTennisPlayerById(id); 
            tennisPlayer.Should().BeEquivalentTo(tennisPlayerExample); 
        }
        [Fact]
        public async Task When_Calling_A_Player_By_Id_Result_Should_Equal_To_Null_If_It_Not_Exist()
        {
            var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            const int id = 128; 
            var tennisPlayer = await tennisNews.GetTennisPlayerById(id); 
            tennisPlayer.Should().Be(null);
            
        }
        [Fact]
        public async Task When_Getting_TennisNewsStatistics_Result_Should_Not_Be_Null()
        {
            var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            var tennisPlayer = await tennisNews.GetTennisNewsStatistics(); 
            tennisPlayer.Should().NotBeNull(); 
        }
        [Fact]
        public async Task When_Getting_TennisNewsStatistics_Result_Of_Best_Country_Should_Equal_To_Country_Example()
        {
            var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            var countryExample = new Country()
            {
                Code = "SRB",
                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png"
            }; 
            var tennisPlayer = await tennisNews.GetTennisNewsStatistics(); 
            tennisPlayer.Country.Should().BeEquivalentTo(countryExample); 
        }
        [Fact]
        public async Task When_Getting_TennisNewsStatistics_Result_Of_Average_height_Should_Equal_To_Average_Height_Example()
        {
            var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            const int  averageHeightExample = 183; 
            var tennisPlayer = await tennisNews.GetTennisNewsStatistics(); 
            tennisPlayer.Height.Should().Be(averageHeightExample); 
        }
        
        [Fact]
        public async Task When_Getting_TennisNewsStatistics_Result_Of_Average_Imc_Should_Equal_To_Average_Imc_Example()
        {
            var remoteClient = new Mock<IHeadToHeadClient>();
            remoteClient
                .Setup(client => client.GetTennisPlayersData())
                .ReturnsAsync(new TennisPlayers()
                {
                    Players = new TennisPlayer[]{
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Novak",
                            Lastname = "Djokovic",
                            ShortName = "N.DJO",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
                                Code = "SRB"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
                            Data = new ()
                            {
                                Rank = 2,
                                Points = 2542,
                                Weight = 80000,
                                Height =188,
                                Age =31,
                                Last = new int[]{1,1,1,1,1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 52,
                            Firstname = "Venus",
                            Lastname = "Williams",
                            ShortName = "V.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Venus.webp",
                            Data = new ()
                            {
                                Rank = 52,
                                Points = 1105,
                                Weight = 74000,
                                Height =185,
                                Age =38,
                                Last = new int[]{0, 1, 0, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 65,
                            Firstname = "Stan",
                            Lastname = "Wawrinka",
                            ShortName = "S.WAW",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Suisse.png",
                                Code = "SUI"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Wawrinka.png",
                            Data = new ()
                            {
                                Rank = 21,
                                Points = 1784,
                                Weight = 81000,
                                Height =183,
                                Age =33,
                                Last = new int[]{1, 1, 1, 0, 1}
                            }
                            
                        },
                        new TennisPlayer()
                        {
                            Id = 102,
                            Firstname = "Serena",
                            Lastname = "Williams",
                            ShortName = "S.WIL",
                            Sex ='F',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/USA.png",
                                Code = "USA"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Serena.png",
                            Data = new ()
                            {
                                Rank = 10,
                                Points = 3521,
                                Weight = 72000,
                                Height =175,
                                Age =37,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        },
                        new TennisPlayer()
                        {
                            Id = 17,
                            Firstname = "Rafael",
                            Lastname = "Nadal",
                            ShortName = "R.NAD",
                            Sex ='M',
                            Country = new()
                            {
                                Picture = "https://data.latelier.co/training/tennis_stats/resources/Espagne.png",
                                Code = "ESP"
                            },
                            Picture = "https://data.latelier.co/training/tennis_stats/resources/Nadal.png",
                            Data = new ()
                            {
                                Rank = 1,
                                Points = 1982,
                                Weight = 85000,
                                Height =185,
                                Age =33,
                                Last = new int[]{0, 1, 1, 1, 0}
                            }
                        }
                    }
                });
            ITennisNews  tennisNews = new TennisNews(remoteClient.Object);
            const double  averageImcExample = 2.3357838995505835; 
            var tennisPlayer = await tennisNews.GetTennisNewsStatistics(); 
            tennisPlayer.Imc.Should().Be(averageImcExample); 
        }
    }
}