using System.Threading.Tasks;
using Domain;

namespace Application
{
    public interface ITennisNews
    {
        Task<TennisPlayers> GetTennisPlayersDescendantly();
        Task<TennisPlayer> GetTennisPlayerById(int id); 
        Task<TennisNewsStatistics> GetTennisNewsStatistics(); 
    }
}