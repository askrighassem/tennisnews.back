using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Values;
using TennisNews.Back.HeadToHead;

namespace Application
{
    public class TennisNews :ITennisNews
    {
        private readonly IHeadToHeadClient _headToHeadClient;

        public TennisNews()
        {
                
        }
        public TennisNews(IHeadToHeadClient headToheadClient)
        {
              _headToHeadClient = headToheadClient;   
        }
        public async Task<TennisPlayers> GetTennisPlayersDescendantly()
        {
           var tennisPlayers= await _headToHeadClient.GetTennisPlayersData(); 
           TennisPlayers  orderedTennisPlayers = new TennisPlayers()
           {
               Players = tennisPlayers.Players.
                   ToList().
                   OrderBy(tennisPlayer=>tennisPlayer.Data.Rank). 
                   ToArray()
           };
           return orderedTennisPlayers ;
        }

        public async Task<TennisPlayer> GetTennisPlayerById(int id)
        {
            var tennisPlayers= await _headToHeadClient.GetTennisPlayersData();
            return tennisPlayers.Players.
                ToList().
                FirstOrDefault(tennisPlayer=>tennisPlayer.Id == id); 
        }

        public async Task<TennisNewsStatistics> GetTennisNewsStatistics()
        {
           
            var tennisPlayers= await _headToHeadClient.GetTennisPlayersData();
            return new TennisNewsStatistics()
            {
                Country =  GetMaxLastRounds(tennisPlayers.Players),
                Height = GetTheAverageHeight(tennisPlayers.Players),
                Imc = GetMedianImc(tennisPlayers.Players)
                
                    
            }; 
        }

        private double GetMedianImc(TennisPlayer[] tennisPlayers)
        {
            var sumOfPlayersImc = tennisPlayers.
                ToList().Sum(tennisPlayer=>CalculateImc(tennisPlayer.Data.Height,tennisPlayer.Data.Weight));
            return sumOfPlayersImc / tennisPlayers.ToList().Count();
        }

        private double CalculateImc(int dataHeight, int dataWeight)
        {
           return dataWeight / Math.Pow(dataHeight,2); 
        }

        private decimal GetTheAverageHeight(TennisPlayer[] tennisPlayers)
        {
            var sumOfPlayersHeight = tennisPlayers.
                ToList().Sum(tennisPlayer=>tennisPlayer.Data.Height);   
            return sumOfPlayersHeight / tennisPlayers.ToList().Count();
        }

        private Country GetMaxLastRounds(TennisPlayer[] tennisPlayers)
        {
            var currentMaximum = tennisPlayers.
                ToList().
                Max( tennisPlayer => tennisPlayer.Data.Last.Sum());
            return tennisPlayers.
                ToList().
                FirstOrDefault(tennisPlayer=>tennisPlayer.Data.Last.Sum() == currentMaximum)
                ?.Country;
                
        }
    }
}