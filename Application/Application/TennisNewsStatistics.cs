using Domain.Values;

namespace Application
{
    public sealed class TennisNewsStatistics
    {
        public Country Country{get;set;}
        public double Imc {get;set;}
        public decimal Height {get;set;}
    }
}