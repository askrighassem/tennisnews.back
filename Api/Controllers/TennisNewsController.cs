using System.Threading.Tasks;
using Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TennisNews.Back.HeadToHead;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/TennisNews")]
    public class TennisNewsController :ControllerBase
    {
        
        private readonly ITennisNews _tennisNews; 

        public TennisNewsController(
             
            ITennisNews tennisNews)
        {
           
            _tennisNews = tennisNews; 
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getTennisPlayers")]
        public async Task<IActionResult> GetTennisPlayers()
        {
            var tennisPlayersData = await _tennisNews.GetTennisPlayersDescendantly();
            return Ok(tennisPlayersData);
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("getTennisPlayerById")]
        public async Task<IActionResult> GetTennisPlayerById(int id)
        {
            var tennisPlayer = await _tennisNews.GetTennisPlayerById(id);
            if (tennisPlayer is null)
            {
                return NotFound(); 
            }
            return Ok(tennisPlayer); 
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("getTennisPlayersStatistics")]
        public async Task<IActionResult> GetTennisPlayersStatistics()
        {
            var tennisPlayer = await _tennisNews.GetTennisNewsStatistics();
            return Ok(tennisPlayer); 
        }

    }
}